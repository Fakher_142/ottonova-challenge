import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io } from "socket.io-client";
import { environment } from '../../environments/environment';
import { Event } from '../models/event';
import { HeaderMessage } from '../models/header-message';

@Injectable({
  providedIn: 'root',
})
export class WebSocketService {
  private socket;

  constructor() {
    this.socket = io(environment.apiUrl);
  }

  fromEvent<T extends HeaderMessage>(type: Event): Observable<T> {
    return new Observable(observer => {
      this.socket.on(type, (data: T) => {
        console.log('Received', type);
        console.log(data);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  emit(type: Event, message: HeaderMessage): void {
    console.log('Emitted', type);
    console.log(message);
    this.socket.emit(type, message);
  }
}