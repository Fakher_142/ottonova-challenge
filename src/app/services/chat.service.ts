import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import {map,tap} from 'rxjs/operators'
import { Author } from '../models/author';
import { BodyMessage } from '../models/body-message';
import { Command } from '../models/command';
import { Event } from '../models/event';
import { HeaderMessage } from '../models/header-message';
import { ICommand } from '../models/ICommand';
import { Message } from '../models/message';
import { Response } from '../models/response';
import { AuthService } from './auth.service';
import { WebSocketService } from './web-socket.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private subs: Subscription[] = []
  private authors = new Set<string>([Author.Bot]);
  private currentAuthor = Author.Bot as string;
  private messages: Array<Message> = [];
  private authorsSubject = new BehaviorSubject<string[]>([this.currentAuthor]);
  private messagesSubject = new BehaviorSubject<Message[]>(this.messages);
  private currentAuthorSubject = new BehaviorSubject<string>(this.currentAuthor);

  constructor(private socket: WebSocketService, private authService: AuthService) {
    // Initialize in constructor for simplicity
    this.subs = [
      this.socket
      .fromEvent<BodyMessage>(Event.Message).pipe(
          tap(data => this.handleAuthor(data)),
          tap(data => this.messages.push(
            new Message(Author.Bot, Author.Bot, {
              type: Command.Message,
              data: data.message,
            }))),
          tap(() => this.messagesSubject.next(this.messages)),
        ).subscribe(),
      this.socket
        .fromEvent<ICommand>(Event.Command).pipe(
          tap(data => this.handleAuthor(data)),
          tap(data => this.messages.push(new Message(data.author as Author, Author.Bot, data.command))),
          tap(() => this.messagesSubject.next(this.messages)),
        ).subscribe(),
    ];
  }

  getMessages(): Observable<Message[]> {
    return this.messagesSubject
      .asObservable()
      .pipe(
        map(messages => messages.filter(message => [this.currentAuthor, this.authService.user].includes(message.author)))
      );
  }

  getAuthors(): Observable<string[]> {
    return this.authorsSubject.asObservable();
  }

  getCurrentAuthor(): Observable<string> {
    return this.currentAuthorSubject.asObservable();
  }

  sendMessage(message: Response): void {
    this.handleResponseMessage(message);

    if (message.type !== Command.Message) {
      return;
    }

    const toEmit = {
      author: this.authService.user,
      message: message.text,
    };
    this.socket.emit(Event.Message, toEmit);
  }

  // send some command in order to receive some random command
  sendCommand(): void {
    this.socket.emit(Event.Command, {
      author: Author.Client,
      command: {
        type: Command.Date,
        data: new Date().toISOString(),
      }
    } as ICommand);
  }

  private handleAuthor(message: HeaderMessage): void {
    if (!this.authors.size) {
      this.currentAuthor = message.author;
      this.currentAuthorSubject.next(message.author);
    }

    this.authors.add(message.author);

    if (this.authors.has(message.author)) {
      this.authorsSubject.next(Array.from(this.authors));
    }
  }

  private handleResponseMessage(message: Response): void {
    this.messages = this.messages.filter(x => x.id !== message.id);

    this.messages.push(
      new Message(Author.Client, this.authService.user, {
        type: Command.Message,
        data: message.text,
      }));

    this.messagesSubject.next(this.messages);
  }
}
