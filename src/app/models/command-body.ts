import { Command } from "./command";

export interface CommandBody {
    type: Command;
    data: string | Location | number[] | string[];
}
