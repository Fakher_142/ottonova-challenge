export enum Event {
    Message = 'message',
    Command = 'command',
}
