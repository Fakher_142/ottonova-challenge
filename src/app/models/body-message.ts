import { HeaderMessage } from "./header-message";

export interface BodyMessage extends HeaderMessage {
    message: string;
}
