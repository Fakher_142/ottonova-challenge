import { Author } from "./author";
import { CommandBody } from "./command-body";
import { HeaderMessage } from "./header-message";

export class Message implements HeaderMessage {
    id = new Date().getTime();

  constructor(public originator: Author, public author: string, public payload: CommandBody) { }
}
