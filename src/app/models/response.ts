import { Command } from "./command";

export interface Response {
    id: number;
    text: string;
    type: Command;
  }
  