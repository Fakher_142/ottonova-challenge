export enum Command {
    Date = 'date',
    Map = 'map',
    Rate = 'rate',
    Complete = 'complete',
    Message = 'message',
}
