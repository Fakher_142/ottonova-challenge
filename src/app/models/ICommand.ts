import { CommandBody } from "./command-body";
import { HeaderMessage } from "./header-message";

export interface ICommand extends HeaderMessage {
    command:CommandBody
}
