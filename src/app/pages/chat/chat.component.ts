import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Message } from 'src/app/models/message';
import { Response } from 'src/app/models/response';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent {
  authors$: Observable<string[]> = this.service.getAuthors();
  messages$: Observable<Message[]> = this.service.getMessages();
  currentAuthor$: Observable<string> = this.service.getCurrentAuthor();

  constructor(private service: ChatService, private authService: AuthService) { }

  onSendMessage(message: Response): void {
    this.service.sendMessage(message);
  }

  onSignout(): void {
    this.authService.logout();
  }

  sendCommand(): void {
    this.service.sendCommand();
  }
}