import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Message } from 'src/app/models/message';
import { Response } from 'src/app/models/response';

@Component({
  selector: 'app-dialogue',
  templateUrl: './dialogue.component.html',
  styleUrls: ['./dialogue.component.scss']
})
export class DialogueComponent{
  @Input() author: string;
  @Input() messages: Message[] = [];

  @Output() sendMessage = new EventEmitter<Response>();
  @Output() sendCommand = new EventEmitter<void>();
  @Output() showList = new EventEmitter<void>();
}
