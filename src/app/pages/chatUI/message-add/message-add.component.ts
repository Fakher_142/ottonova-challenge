import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { Command } from 'src/app/models/command';
import { Response } from 'src/app/models/response';

@Component({
  selector: 'app-message-add',
  templateUrl: './message-add.component.html',
  styleUrls: ['./message-add.component.scss']
})
export class MessageAddComponent {
  @Output() sendMessage = new EventEmitter<Response>();
  @Output() sendCommand = new EventEmitter<void>();

  @ViewChild('input') input: ElementRef;

  onSend(text: string): void {
    if (!text) {
      return;
    }

    this.sendMessage.emit({ id: Date.now(), text, type: Command.Message });
    this.input.nativeElement.value = '';
  }
}

