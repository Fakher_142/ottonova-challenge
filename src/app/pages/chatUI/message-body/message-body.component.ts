import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { AfterViewChecked, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Author } from 'src/app/models/author';
import { Command } from 'src/app/models/command';
import { Response } from 'src/app/models/response';

@Component({
  selector: 'app-message-body',
  templateUrl: './message-body.component.html',
  styleUrls: ['./message-body.component.scss']
})
export class MessageBodyComponent implements AfterViewChecked {
  @Input() messages: Message[] = [];

  @Output() sendMessage = new EventEmitter<Response>();

  @ViewChild('messsageWrapper') messageContainer: ElementRef;

  AuthorType = Author;
  CommandType = Command;

  ngAfterViewChecked(): void {
    const elem: HTMLElement = this.messageContainer.nativeElement;
    elem.scrollTop = elem.scrollHeight;
  }
}
