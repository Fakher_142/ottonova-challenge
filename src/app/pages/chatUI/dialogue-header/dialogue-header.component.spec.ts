import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogueHeaderComponent } from './dialogue-header.component';

describe('DialogueHeaderComponent', () => {
  let component: DialogueHeaderComponent;
  let fixture: ComponentFixture<DialogueHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogueHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogueHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
