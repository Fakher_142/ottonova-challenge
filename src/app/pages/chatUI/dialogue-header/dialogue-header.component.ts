import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dialogue-header',
  templateUrl: './dialogue-header.component.html',
  styleUrls: ['./dialogue-header.component.scss']
})
export class DialogueHeaderComponent {
  @Input() model: string;

  @Output() showList = new EventEmitter<void>();
}
