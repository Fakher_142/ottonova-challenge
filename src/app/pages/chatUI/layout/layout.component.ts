import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {
  private mobileQueryListener: () => void;
  private eventType = 'change';
  private subs: Subscription[] = [];

  mobileQuery: MediaQueryList;
  showListSub = new Subject();

  @Input() mode = 'List';
  @Input() conversationListItemTemplate: TemplateRef<any>;
  @Input() conversationTemplate: TemplateRef<any>;
  @Input() conversationListContext: any;
  @Input() conversationContext: any;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia(`(max-width: ${environment.mobileSizePx}px)`);
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener(this.eventType, this.mobileQueryListener);
    this.subs = [this.showListSub.subscribe(() => this.showList())];
  }

  showDetails(): void {
    this.mode = 'Details';
  }

  showList(): void {
    this.mode = 'List';
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener(this.eventType, this.mobileQueryListener);
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
