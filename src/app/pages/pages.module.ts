import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompleteComponent } from './widgets/complete/complete.component';
import { DateComponent } from './widgets/date/date.component';
import { MapComponent } from './widgets/map/map.component';
import { RateComponent } from './widgets/rate/rate.component';
import { LoginComponent } from './login/login.component';
import { ChatComponent } from './chat/chat.component';
import { AuthGuard } from '../core/auth.guard';
import { MaterialModule } from '../material/material.module';
import { StarRatingModule } from 'angular-star-rating';
import { GoogleMapsModule } from '@angular/google-maps';
import { PageLayoutComponent } from '../shared/page-layout/page-layout.component';
import { HeaderComponent } from '../shared/header/header.component';
import { PageContainerComponent } from '../shared/page-container/page-container.component';
import { RouterModule } from '@angular/router';
import { AuthorsComponent } from './chatUI/authors/authors.component';
import { LayoutComponent } from './chatUI/layout/layout.component';
import { DialogueComponent } from './chatUI/dialogue/dialogue.component';
import { DialogueHeaderComponent } from './chatUI/dialogue-header/dialogue-header.component';
import { MessageBodyComponent } from './chatUI/message-body/message-body.component';
import { MessageAddComponent } from './chatUI/message-add/message-add.component';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';


const routes = [
  {
    path: '',
    component: ChatComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [
    ChatComponent,
    PageLayoutComponent,
    CompleteComponent,
    DateComponent,
    MapComponent,
    RateComponent,
    LoginComponent,
    HeaderComponent,
    PageContainerComponent,
    AuthorsComponent,
    LayoutComponent,
    DialogueComponent,
    DialogueHeaderComponent,
    MessageBodyComponent,
    MessageAddComponent,

  ],
  imports: [
    CommonModule,
    MaterialModule,
    StarRatingModule.forRoot(),
    NgbRatingModule,
    RouterModule.forChild(routes),
    GoogleMapsModule,
  ]
})
export class PagesModule { }
