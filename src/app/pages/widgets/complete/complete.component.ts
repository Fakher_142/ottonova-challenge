import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Command } from 'src/app/models/command';

@Component({
  selector: 'app-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss']
})
export class CompleteComponent {
  @Input() id: number;
  @Input() buttons: string[];

  @Output() sendMessage = new EventEmitter();

  complete(button: string): void {
    const text = button.toLowerCase() === 'yes' ? 'You closed the conversation.' : 'Enter your message...';
    this.sendMessage.emit({ id: this.id, text, type: Command.Complete });
  }
}
