import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Command } from 'src/app/models/command';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent{
  @Input() id: number;
  @Input() model: string;

  allDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

  get currentDay(): number {
    return this.model ? new Date(this.model).getDay() - 1 : 0;
  }

  get days(): string[] {
    return this.allDays.slice(this.currentDay).concat(this.allDays.slice(0, this.currentDay));
  }

  @Output() sendMessage = new EventEmitter();

  onClick(day: string): void {
    this.sendMessage.emit({ id: this.id, text: `${day} is your day.`, type: Command.Date });
  }
}
