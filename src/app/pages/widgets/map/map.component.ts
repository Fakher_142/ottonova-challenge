import { Component, Input } from '@angular/core';
import {Location} from '../../../models/location'
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent{
  marker:any;
  @Input() id: number;
  @Input() model: Location;
  constructor(){
    this.marker =  {
    label: {
      color: 'red',
      text: 'Marker label 1',
    },
    title: 'Marker title 1' ,
    options: { animation: google.maps.Animation.BOUNCE },
    }
  }
}

