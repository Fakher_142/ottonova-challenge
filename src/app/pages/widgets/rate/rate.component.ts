import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Command } from 'src/app/models/command';
import { Response } from 'src/app/models/response';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})
export class RateComponent {
  currentRate:number;
  @Input() id: number;
  @Input() model = [0, 10];

  @Output() sendMessage = new EventEmitter<Response>();

  onClick(event:number): void {
    console.log(event);
    this.currentRate = event
    setTimeout(() => this.sendMessage.emit({
      id: this.id,
      text: `You rated us: ${event}. Thank you!`,
      type: Command.Rate,
    }), 500);
  }
}
